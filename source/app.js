/*### BEGIN LICENSE
# Copyright (C) 2011 Stephan Tetzel <info@zefanjas.de>
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE*/

enyo.kind({
    name: "BibleZ.App",
    kind: enyo.VFlexBox,
    components: [
        {kind: "ApplicationEvents", onUnload: "saveSettings"},
        {kind: "ApplicationEvents", onWindowParamsChange: "launchParamsChanged"},
        {kind: "PalmService", service: "palm://com.palm.applicationManager/", method: "open"},
        /*{name: "swordApi", kind: "BibleZ.SwordApi",
            onGetSyncConfig: "handleGetSyncConfig",
            onRefreshedSource: "handleRefreshedSource",
            onGetRemoteModules: "handleGetRemoteModules",
            onInstalledModule: "handleInstalledModule",
            onProgress: "handleProgress",
            onGetResults: "handleGetResults",
            onPluginError: "showError"
        }, */
        {kind: "AppMenu", components: [
            {caption: $L("Module Manager"), onclick: "openModuleMgr"},
            {caption: $L("Preferences"), onclick: "openPrefs"},
            {caption: $L("Disable Sleep Mode"), onclick: "setDim", dim: true},
            {caption: $L("Help"), onclick: "openHelp"},
            {caption: $L("Leave A Review"), onclick: "openReview"},
            {caption: $L("About"), onclick: "openAbout"}
        ]},
        {name: "biblezAbout", kind: "BibleZ.About"},
        {name: "errorDialog", kind: "BibleZ.Error"},
        {name: "mainPane", flex: 1, kind: "Pane", transitionKind: "enyo.transitions.Simple", onSelectView: "viewSelected", components: [
            {name: "start", kind: "App.Start"},
            {name: "welcome", kind: "App.Welcome", onOpenModMan: "openModuleMgr"},
            {name: "verseView", kind: "HFlexBox",/* className: "scroller-background", */ components: [
                {name: "mainView", kind: "App.MainView", flex: 1,
                    onGetModules: "handleGetModules",
                    onGetBooknames: "handleGetBooknames",
                    onGetVerses: "handleGetVerses",
                    onGetStrong: "handleGetStrong",
                    onGoToMain: "goToMainView",
                    onWelcome: "goToWelcome",
                    onMainVerse: "handleMainVerse",
                    onGetVMax: "handleGetVMax",
                    onSync: "handleSyncSplitView",
                    onSearch: "handleSearch",
                    onGetSplitBookmarks: "handleGetSplitBookmarks",
                    onGetSplitNotes: "handleGetSplitNotes",
                    onGetSplitHighlights: "handleGetSplitHighlights"
                },
                {name: "splitPane", kind: "Pane", showing: false, flex: 1, components: [
                    {name: "splitView", kind: "App.MainView", className: "split-view", view: "split",
                        onGetModules: "handleGetModules",
                        onGetBooknames: "handleGetBooknames",
                        onGetVerses: "handleGetVerses",
                        onGetStrong: "handleGetStrong",
                        onGoToMain: "goToMainView",
                        onWelcome: "goToWelcome",
                        onGetVMax: "handleGetVMax",
                        onSplitVerse: "handleSplitVerse",
                        onSync: "handleSyncSplitView",
                        onSearch: "handleSearch"
                    }
                ]}
            ]},

            {name: "modManView", kind: "App.ModMan",
                onUntar: "untarModules",
                onInstallModule: "handleInstallModule",
                onGetDetails: "getModuleDetails",
                onRemove: "removeModule",
                onGetSync: "getSyncConfig",
                onGetRepos: "handleRemoteSources",
                onRefreshSource: "getRefreshRemoteSource",
                onListModules: "getRemoteModules",
                onGetModules: "handleGetModules",
                onModulesChanged: "handleModulesChanged",
                onBack: "goToMainView"
            },
            {name: "prefs", kind: "BibleZ.Prefs",
                onBack: "goToMainView",
                onBgChange: "changeBackground",
                onLbChange: "changeLinebreak",
                onScrollChange: "changeScrolling",
                onGlobalOption: "changeGlobalOption"
            }
        ]},
        {name: "btSplit", content: "", className: "split-button", showing: true, allowDrag: true,
            onclick: "openSecondViewClicked",
            ondragstart: "seperatorDragStart",
            ondrag: "seperatorDrag",
            ondragfinish: "seperatorDragFinish"
        }
    ],

    published: {
        launchParams: null
    },

    splitWidth: 0,

    create: function() {
        this.inherited(arguments);
        api.createDB();

        enyo.keyboard.setResizesWindow(false);
        biblez.isOpen = false;
        biblez.openedSplitView = false;
        biblez.reloadModules = false;

        //LOAD PREFERENCES
        storage.get("mainModule", enyo.bind(this, function (data) {
            if (data)
                this.$.mainView.setCurrentModule(data.value);
        }));
        storage.get("splitModule", enyo.bind(this, function (data) {
            if (data)
                this.$.splitView.setCurrentModule(data.value);
        }));
    },


    //JUST TYPE
    launchParamsChanged: function(inSender){
        if (enyo.windowParams.search) {
            //enyo.log(enyo.windowParams.search);
            this.$.mainView.getVerses(decodeURIComponent(enyo.windowParams.search));
        }
    },

    //SWORD API CALLS

    handleGetModules: function (inSender) {
        swordApi.getInstalledModules(enyo.bind(inSender, inSender.handleGetModules));
    },

    handleModulesChanged: function (inSender) {
        this.$.mainView.handleGetModules(biblez.modules, true);
        this.$.splitView.handleGetModules(biblez.modules, true);
    },

    handleGetBooknames: function (inSender, module) {
        swordApi.getBooknames(enyo.bind(inSender, inSender.handleGetBooknames), module);
    },

    handleGetVerses: function (inSender, passage, module, single) {
        //enyo.log(inSender, passage, module);
        swordApi.getVerses(enyo.bind(inSender, inSender.handleGetVerses), passage, module, single);
    },

    handleGetStrong: function (inSender, passage, module) {
        //enyo.log(inSender, passage, module);
        swordApi.getStrong(enyo.bind(inSender, inSender.handleGetStrong), module, passage);
    },

    handleGetVMax: function (inSender, passage) {
        //enyo.log(inSender, passage);
        swordApi.getVMax(enyo.bind(inSender, inSender.handleGetVMax), passage);
    },

    handleRemoteSources: function (inSender) {
        swordApi.getRemoteSources(enyo.bind(inSender, inSender.handleGotRepos));
    },

    getSyncConfig: function (inSender) {
        swordApi.getSyncConfig();
    },

    handleGetSyncConfig: function (inSender, response) {
        this.$.modManView.handleGotSyncConfig(response);
    },

    getRefreshRemoteSource: function (inSender) {
        swordApi.getRefreshRemoteSource();
    },

    handleRefreshedSource: function (inSender, response) {
        enyo.log(response);
        if (enyo.json.parse(response).returnValue)
            this.getRemoteModules();
        else
            this.showError(null, enyo.json.parse(response).message);
            this.$.modManView.stopSpinner();
    },

    getRemoteModules: function (inRepo) {
        swordApi.getRemoteModules(inRepo);
    },

    handleGetRemoteModules: function (inSender, modules) {
        enyo.log("Got all available modules...");
        api.prepareModules(enyo.json.parse(modules), enyo.bind(this.$.modManView, this.$.modManView.getLang));
    },

    getModuleDetails: function (inSender) {
        swordApi.getModuleDetails(enyo.bind(inSender, inSender.showDetails), inSender.getModuleToInstall());
    },

    handleInstallModule: function (inSender) {
        swordApi.installModule(inSender.getModuleToInstall());
    },

    handleInstalledModule: function (inSender, response) {
        this.$.modManView.handleInstalledModule(response);
    },

    handleProgress: function (inSender, response) {
        this.$.modManView.setInstallProgress(response);
    },

    removeModule: function (inSender) {
        swordApi.uninstallModule(enyo.bind(inSender, inSender.handleRemove), inSender.getModuleToRemove().name);
    },

    handleSearch: function (inSender, module, searchTerm, searchType, searchScope, view) {
        //enyo.log(module, searchTerm, searchType, searchScope, view);
        swordApi.search((module === "mainModule") ? this.$.mainView.getCurrentModule().name : module, searchTerm, searchScope, searchType, view);
    },

    handleGetResults: function (inSender, results, view) {
        if (view === "main")
            this.$.mainView.handleSearchResults(results, view);
        else
            this.$.splitView.handleSearchResults(results, view);
    },

    //PANE STUFF

    goToMainView: function (inSender, inWait) {
        if (biblez.welcome)
            this.$.mainPane.back();
        else {
            if (!inWait || typeof inWait !== "boolean")
                this.$.mainPane.selectViewByName("verseView");
        }
    },

    goToWelcome: function () {
        biblez.welcome = true;
        this.$.mainPane.selectViewByName("welcome");
    },

    handleMainVerse: function (inSender, passage, verse) {
        if(!this.$.splitPane.showing) {
            biblez.openedSplitView = true;
            this.openSecondView();
        }
        this.$.splitView.getVerses(passage, verse);
    },

    viewSelected: function(inSender, inView, inPreviousView) {
        this.$.btSplit.hide();
        if (inView.name == "modManView") {
            this.$.modManView.getRepos();
        } else if (inView.name == "verseView") {
            this.$.btSplit.show();
            if(biblez.modules.length !== 0) {
                if (this.scrollingChanged) {
                    this.$.mainView.changeScrolling(biblez.scrollHorizontal);
                    this.$.splitView.changeScrolling(biblez.scrollHorizontal);
                    this.scrollingChanged = false;
                }
                this.$.mainView.getVerses();
            }
        }
    },

    openSecondViewClicked: function () {
        enyo.nextTick(this, this.openSecondView);
    },

    openSecondView: function (inSender, inEvent) {
        //enyo.log("Tapped + Button");
        if (!this.drag) {
            if (this.$.splitPane.showing) {
                this.$.splitPane.hide();
                this.$.btSplit.setClassName("split-button");
                this.$.btSplit.addStyles("right: 0px;");
                this.$.mainView.resizeHandler(true);
            } else {
                this.$.splitPane.show();
                this.$.btSplit.setClassName("split-button-middle");
                var right = (this.splitWidth === 0) ? window.innerWidth/2-31 : this.splitWidth;
                this.$.btSplit.addStyles("right: " +  right + "px;");
                if (!biblez.openedSplitView) {
                    this.$.splitView.getVerses(this.$.mainView.getPassage().passage);
                    biblez.openedSplitView = true;
                }

                this.$.splitView.resizeHandler();
                this.$.mainView.resizeHandler(true);
            }
        } else {
            this.drag = false;
        }
    },

    seperatorDragStart: function (inSender, inEvent) {
        if (Math.abs(inEvent.dx) > Math.abs(inEvent.dy) && this.$.splitPane.showing) {
            this.drag = true;
            return true;
        }
    },

    seperatorDrag: function (inSender, inEvent) {
        if (Math.abs(inEvent.dx) > Math.abs(inEvent.dy) && this.$.splitPane.showing) {
            this.splitWidth = window.innerWidth - inEvent.pageX -31;
            this.$.btSplit.addStyles("right: " + this.splitWidth + "px;");
        }
    },

    seperatorDragFinish: function (inSender, inEvent) {
        if (this.drag) {
            var left = inEvent.pageX;
            var right = window.innerWidth - inEvent.pageX;
            var ggt = this.getGGT(left, right);
            this.$.mainView.addStyles("-webkit-box-flex: " + ggt.left + ";");
            this.$.splitPane.addStyles("-webkit-box-flex: " + ggt.right + ";");

            this.$.splitView.resizeHandler(true);
            this.$.mainView.resizeHandler(true);
        }
    },

    getGGT: function (a,b) {
        var a1=a;
        var b1=b;
        while (a1!=b1)
            if (a1>b1)
                a1=a1-b1;
            else
                b1=b1-a1;
        return {"left": a/a1, "right": b/a1};
    },

    //SPLIT VIEW

    handleSyncSplitView: function (inSender, inEvent) {
        //enyo.log("HandleSync", this.$.splitView.getSync() && this.$.splitPane.showing);
        if (this.$.splitView.getSync() && this.$.splitPane.showing) {
            if (inSender.goPrev) {
                this.$.splitView.setGoPrev(true);
                this.$.splitView.getVerses(this.$.mainView.getPassage().passage);
                inSender.setGoPrev(false);
            } else
                this.$.splitView.getVerses(this.$.mainView.getPassage().passage);
        }
    },

    handleSplitVerse: function (inSender, passage, verse) {
        this.$.mainView.getVerses(passage, verse);
    },

    handleGetSplitBookmarks: function (inSender) {
        this.$.splitView.getBookmarks((this.$.splitPane.showing) ? false : true);
    },

    handleGetSplitNotes: function (inSender) {
        this.$.splitView.getNotes((this.$.splitPane.showing) ? false : true);
    },

    handleGetSplitHighlights: function (inSender) {
        this.$.splitView.getHighlights((this.$.splitPane.showing) ? false : true);
    },

    //APP MENU

    openModuleMgr: function (inSender, inEvent) {
        this.$.mainPane.selectViewByName("modManView");
    },

    openPrefs: function (inSender, inEvent) {
        this.$.mainPane.selectViewByName("prefs");
    },

    setDim: function (inSender, inEvent) {
        if (inSender.dim) {
            inSender.setCaption($L("Enable Sleep Mode"));
            inSender.dim = false;
            enyo.windows.setWindowProperties(window, {blockScreenTimeout: true});
        } else {
            inSender.setCaption($L("Disable Sleep Mode"));
            inSender.dim = true;
            enyo.windows.setWindowProperties(window, {blockScreenTimeout: false});
        }
    },

    openHelp: function () {
        this.$.palmService.call({
            id: 'com.palm.app.browser',
            params: {
                "target": "http://zefanjas.de/biblezpro"
            }
        });
    },

    openReview: function () {
        window.location = "http://developer.palm.com/appredirect/?packageid=de.zefanjas.biblezhdpro";
    },

    openAbout: function ()  {
        this.$.biblezAbout.openAtCenter();
    },

    //PREFS
    changeBackground: function () {
        this.$.mainView.setBackground(biblez.background);
        this.$.splitView.setBackground(biblez.background);
    },

    changeScrolling: function (inSender, inEvent) {
        this.scrollingChanged = true;
    },

    changeGlobalOption: function (inSender, inOption, inValue) {
        //enyo.log(inOption, inValue);
        swordApi.setGlobalOption(enyo.bind(inSender, inSender.handleGlobalOption), inOption, inValue);
    },

    //MISC

    saveSettings: function () {
        enyo.windows.setWindowProperties(window, {blockScreenTimeout: false});
    },

    showError: function (inSender, message) {
        this.$.errorDialog.setError(message);
        this.$.errorDialog.openAtCenter();
    },

    resizeHandler: function() {
        //enyo.log("resized main");
        this.inherited(arguments);
        if (this.$.splitPane.showing) {
            var right = this.$.splitPane.hasNode().clientWidth-29;
            //enyo.log(right);
            this.$.btSplit.addStyles("right: " +  right + "px;");
            this.$.splitView.resizeHandler();
        }
        this.$.mainView.resizeHandler(true);
    }
});