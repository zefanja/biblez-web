/*### BEGIN LICENSE
# Copyright (C) 2011 Stephan Tetzel <info@zefanjas.de>
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE*/

enyo.kind({
	name: "BibleZ.Prefs",
	kind: enyo.VFlexBox,
    events: {
        onBack: "",
        onBgChange: "",
		onLbChange: "",
        onScrollChange: "",
        onGlobalOption: ""
    },
	published: {
		background: "biblez",
		linebreak: false,
        footnotes: true,
        heading: true,
        scrolling: false,
		backupTime: ""
	},
	components: [
		{kind: "FileService", name: "backupService" },
        {name: "filepicker", kind: "FilePicker", extensions: ["json"], fileType:["document"], allowMultiSelect:true, onPickFile: "handleFilePicker"},
        {kind: "Header", components: [
            {kind: "Button", caption: $L("Back"), onclick: "doBack"},
			{kind: "Spacer"},
			{content: $L("Preferences")},
			{kind: "Spacer"}
        ]},
        {kind: "Scroller", flex: 1, components: [
            {kind: "RowGroup", caption: $L("General"), defaultKind: "HFlexBox", style: "margin-left: auto; margin-right: auto;", className: "prefs-container", components: [
                {name: "generalSelector", kind: "ListSelector", label: $L("Background"), onChange: "itemChanged", items: [
                    {caption: $L("Default"), value: "default"},
					{caption: $L("Paper Grayscale"), value: "grayscale"},
                    {caption: $L("Gray"), value: "palm"},
					{caption: $L("Night View"), value: "night"}
                ]},
                {align: "center", components: [
                    {flex: 1, name: "scrolling", content: $L("Scrolling Method")},
                    {name: "toggleScroll", kind: "ToggleButton", onLabel: $L("Horizontal"), offLabel: $L("Vertical"), state: true, onChange: "changeScrolling"}
                ]},
				{align: "center", components: [
					{flex: 1, name: "linebreak", content: $L("Enable Linebreaks")},
					{name: "toggleLB", kind: "ToggleButton", state: this.linebreak, onChange: "changeLinebreak"}
				]},
                {align: "center", components: [
                    {flex: 1, content: $L("Enable Headings")},
                    {name: "toggleHeading", kind: "ToggleButton", state: true, onChange: "changeHeading"}
                ]},
                {align: "center", components: [
                    {flex: 1, content: $L("Enable Book Introductions")},
                    {name: "toggleIntro", kind: "ToggleButton", state: true, onChange: "changeIntro"}
                ]},
                {align: "center", components: [
                    {flex: 1, name: "footnotes", content: $L("Enable Footnotes")},
                    {name: "toggleFN", kind: "ToggleButton", state: true, onChange: "changeFootnote"}
                ]},
                {align: "center", components: [
                    {flex: 1, name: "crossRef", content: $L("Enable Cross References")},
                    {name: "toggleCR", kind: "ToggleButton", state: false, onChange: "changeCrossRef"}
                ]},
                {align: "center", components: [
                    {kind: "VFlexBox", flex: 1, components: [
                        {name: "strongs", content: $L("Enable Strong's Numbers")},
                        {content: $L("You have to install the 'StrongsGreek' and 'StrongsHebrew' module to use this feature!"), style: "font-size: 0.8em;"}
                    ]},
                    {name: "toggleStrongs", kind: "ToggleButton", state: false, onChange: "changeStrongs"}
                ]},
                {align: "center", components: [
                    {flex: 1, content: $L("Words of Christ in Red")},
                    {name: "toggleWoC", kind: "ToggleButton", state: false, onChange: "changeWoC"}
                ]}
            ]},
            {kind: "Group", caption: $L("Custom Fonts"), defaultKind: "HFlexBox", style: "margin-left: auto; margin-right: auto;", className: "prefs-container", components: [
                {kind: "VFlexBox", components: [
                    {content: $L("You need to install your font to '/usr/share/fonts' first! (<a href='http://zefanjas.de/biblez'>more Infos here</a>)."), allowHtml: true, className: "hint-small"},
                    {name: "hebrewInput", kind: "Input", hint: "", onblur: "handleHebrewFont", components: [
                        {content: $L("Hebrew Font"), className: "popup-label"}
                    ]}
                ]},
                {kind: "VFlexBox", components: [
                    {name: "greekInput", kind: "Input", hint: "", onblur: "handleGreekFont", components: [
                        {content: $L("Greek Font"), className: "popup-label"}
                    ]}
                ]}
            ]},
            {kind: "RowGroup", caption: $L("Backup & Restore"), defaultKind: "HFlexBox", style: "margin-left: auto; margin-right: auto;", className: "prefs-container", components: [
				{kind: "VFlexBox", components: [
					{kind: "ActivityButton", name: "btBackup", caption: $L("Backup Data"), onclick: "handleBackup"},
					{content: $L("Backups are stored in '/media/internal/biblez'"), className: "hint-small"}
				]},
                {kind: "VFlexBox", components: [
                    {kind: "ActivityButton", name: "btRestore", caption: $L("Restore Data"), onclick: "openFilePicker"},
                    {content: $L("All your current data will be removed!!!"), className: "hint-small"}
                ]}
            ]},
            {kind: "RowGroup", caption: $L("Restore BibleZ Pro Backup (old webOS Phone version)"), defaultKind: "HFlexBox", style: "margin-left: auto; margin-right: auto;", className: "prefs-container", components: [
                {kind: "VFlexBox", components: [
                    {content: $L("You can import your notes and bookmarks from BibleZ Pro (webOS phone version) to BibleZ HD Pro. Restoring your highlights isn't supported."), className: "hint-small"},
                    {kind: "ActivityButton", name: "btRestoreOld", caption: $L("Import Bookmarks and Notes"), onclick: "handleOldBackup"},
                    {kind: "RichText", name: "oldBackupInput", hint: $L("Paste your backup here"), richContent: false}

                ]}
            ]},
            {kind: "Spacer"}
        ]}
    ],

    create: function () {
        this.inherited(arguments);
        storage.get("scrolling", enyo.bind(this, function (data) {
            if (data) {
                biblez.scrollHorizontal = data.value;
                this.$.toggleScroll.setState(biblez.scrollHorizontal);
            } else {
                biblez.scrollHorizontal = true;
            }
        }));

        storage.get("linebreak", enyo.bind(this, function (data) {
            if (data) {
                biblez.linebreak = data.value;
                this.$.toggleLB.setState(biblez.linebreak);
            }
        }));

        storage.get("intro", enyo.bind(this, function (data) {
            if (data) {
                biblez.intro = data.value;
                this.$.toggleIntro.setState(biblez.intro);
            } else {
                biblez.intro = true;
            }
        }));

        storage.get("heading", enyo.bind(this, function (data) {
            if (data) {
                biblez.heading = data.value;
                this.$.toggleHeading.setState(biblez.heading);
            } else {
                biblez.heading = true;
            }
        }));

        storage.get("footnote", enyo.bind(this, function (data) {
            if (data) {
                biblez.footnote = data.value;
                this.$.toggleFN.setState(biblez.footnote);
            } else {
                biblez.footnote = true;
            }
        }));

        storage.get("crossRef", enyo.bind(this, function (data) {
            if (data) {
                biblez.crossRef = data.value;
                this.$.toggleCR.setState(biblez.crossRef);
            } else {
                biblez.crossRef = false;
            }
        }));

        storage.get("strongs", enyo.bind(this, function (data) {
            if (data) {
                biblez.strongs = data.value;
                this.$.toggleStrongs.setState(biblez.strongs);
            } else {
                biblez.strongs = false;
            }
        }));

        storage.get("background", enyo.bind(this, function (data) {
            if (data) {
                biblez.background = data.value;
                this.$.generalSelector.setValue(biblez.background);
            } else {
                biblez.background = "default";
            }
        }));

        storage.get("hebrewFont", enyo.bind(this, function (data) {
            if (data) {
                biblez.hebrewFont = data.value;
                this.$.hebrewInput.setValue(biblez.hebrewFont.replace(/'/g, ""));
            }
        }));

        storage.get("greekFont", enyo.bind(this, function (data) {
            if (data) {
                biblez.greekFont = data.value;
                this.$.greekInput.setValue(biblez.greekFont.replace(/'/g, ""));
            }
        }));

        storage.get("woc", enyo.bind(this, function (data) {
            if (data) {
                this.doGlobalOption("Words of Christ in Red", data.value);
                this.$.greekInput.setValue(biblez.greekFont.replace(/'/g, ""));
                this.$.toggleWoC.setState((data.value == "On") ? true : false);
            }
        }));
    },

    itemChanged: function(inSender, inValue, inOldValue) {
        storage.save({key: "background", value: inValue}, function (data) {
            enyo.log("Saved background");
        });
        biblez.background = inValue;
        this.doBgChange();
    },

    setBgItem: function (value) {
        this.background = value;
        this.$.generalSelector.setValue(value);
    },

    changeScrolling: function (inSender, inState) {
        storage.save({key: "scrolling", value: inState}, function (data) {
            enyo.log("Saved scrolling");
        });
        biblez.scrollHorizontal = inState;
        this.doScrollChange();
    },

	changeLinebreak: function (inSender, inState) {
		//enyo.log(inState);
        storage.save({key: "linebreak", value: inState}, function (data) {
            enyo.log("Saved linebreak");
        });
		biblez.linebreak = inState;
	},

    changeHeading: function (inSender, inState) {
        //enyo.log(inState);
        storage.save({key: "heading", value: inState}, function (data) {
            enyo.log("Saved heading");
        });
        biblez.heading = inState;
    },

    changeIntro: function (inSender, inState) {
        //enyo.log(inState);
        storage.save({key: "intro", value: inState}, function (data) {
            enyo.log("Saved intro");
        });
        biblez.intro = inState;
    },

    changeFootnote: function (inSender, inState) {
        //enyo.log(inState);
        storage.save({key: "footnote", value: inState}, function (data) {
            enyo.log("Saved footnote");
        });
        biblez.footnote = inState;
    },

    changeCrossRef: function (inSender, inState) {
        //enyo.log(inState);
        storage.save({key: "crossRef", value: inState}, function (data) {
            enyo.log("Saved crossRef");
        });
        biblez.crossRef = inState;
    },

    changeStrongs: function (inSender, inState) {
        //enyo.log(inState);
        storage.save({key: "strongs", value: inState}, function (data) {
            enyo.log("Saved strongs");
        });
        biblez.strongs = inState;
    },

    changeWoC: function (inSender, inState) {
        //enyo.log(inState);
        var value = (inState) ? "On" : "Off";
        storage.save({key: "woc", value: value}, function (data) {
            enyo.log("Saved WoC");
        });
        this.doGlobalOption("Words of Christ in Red", value);
    },

    scrollingChanged: function (inSender, inEvent) {
        this.$.toggleScroll.setState(this.scrolling);
    },

	linebreakChanged: function (inSender, inEvent) {
		this.$.toggleLB.setState(this.linebreak);
	},

    headingChanged: function (inSender, inEvent) {
        this.$.toggleHeading.setState(this.heading);
    },

    footnotesChanged: function (inSender, inEvent) {
        this.$.toggleFN.setState(this.footnotes);
    },

    handleHebrewFont: function (inSender, inEvent) {
        //enyo.log(inSender.getValue());
        biblez.hebrewFont = "'" + inSender.getValue() + "'";
        storage.save({key: "hebrewFont", value: biblez.hebrewFont}, function (data) {
            enyo.log("Saved hebrewFont");
        });
    },

    handleGreekFont: function (inSender, inEvent) {
        //enyo.log(inSender.getValue());
        biblez.greekFont = "'" + inSender.getValue() + "'";
        storage.save({key: "greekFont", value: biblez.greekFont}, function (data) {
            enyo.log("Saved greekFont");
        });
    },

    setCustomFonts: function (hebrew, greek) {
        this.$.hebrewInput.setValue(hebrew.replace(/'/g, ""));
        this.$.greekInput.setValue(greek.replace(/'/g, ""));
    },

	handleBackup: function (inSender, inEvent) {
		this.$.btBackup.setActive(true);
		var time = new Date();
		this.backupTime = time.getFullYear().toString() + (time.getMonth() + 1).toString() + time.getDate().toString();
		//enyo.log(this.backupTime, time.getFullYear(), time.getMonth() + 1, time.getDate());
		api.getNotes(-1,-1,enyo.bind(this, this.callBackupNotes));
		api.getBookmarks(-1,-1,enyo.bind(this, this.callBackupBookmarks));
		api.getHighlights(-1,-1,enyo.bind(this, this.callBackupHighlights));
	},

	callBackupNotes: function (content) {
		this.$.backupService.writeFile("/media/internal/biblez/biblezNotes-" + this.backupTime + ".json", enyo.json.stringify(content), enyo.bind(this, this.callbackBackup, $L("Notes")));
	},

	callBackupBookmarks: function (content) {
		this.$.backupService.writeFile("/media/internal/biblez/biblezBookmarks-" + this.backupTime + ".json", enyo.json.stringify(content), enyo.bind(this, this.callbackBackup, $L("Bookmarks")));
	},

	callBackupHighlights: function (content) {
		this.$.backupService.writeFile("/media/internal/biblez/biblezHighlights-" + this.backupTime + ".json", enyo.json.stringify(content), enyo.bind(this, this.callbackBackup, $L("Highlights")));
	},

	callbackBackup: function (inType, inResponse) {
		this.$.btBackup.setActive(false);
		//enyo.log("RESPONSE:", inResponse);
		if (inResponse.returnValue) {
			enyo.windows.addBannerMessage($L("Backuped") + " " + inType, enyo.json.stringify({}));
		}
	},

    handleOldBackup: function () {
        this.oldData = enyo.json.parse(PalmSystem.decrypt("JesusIsTheLord", this.$.oldBackupInput.getValue()));
        if (this.oldData.notes && this.oldData.notes.length !== 0) {
            api.getNotes(-1,-1,enyo.bind(this, this.callbackOldNotes));
        }

        if (this.oldData.bookmarks && this.oldData.bookmarks.length !== 0) {
            api.getBookmarks(-1,-1,enyo.bind(this, this.callbackOldBookmarks));
        }
    },

    callbackOldNotes: function (data) {
        var tmpNotes = [];
        var found = 0;
        for (var i=0; i<this.oldData.notes.length; i++) {
            found = 0;
            for (var j=0; j<data.length; j++) {
                if (this.oldData.notes[i].bnumber === data[j].bnumber && this.oldData.notes[i].cnumber === data[j].cnumber && this.oldData.notes[i].vnumber === data[j].vnumber) {
                    found = j;
                } else {

                }
            }
            if (found === 0)
                data.push(this.oldData.notes[i]);
            else {
                data[found].note = data[found].note += "<p> --- Restored Note ----- </p>" + this.oldData.notes[i].note;
            }
        }
        api.restoreNotes(data, enyo.bind(this, this.callbackRestore, $L("Old Notes")));
        this.$.oldBackupInput.setValue("");
    },

    callbackOldBookmarks: function (data) {
        var found = 0;
        for (var i=0; i<this.oldData.bookmarks.length; i++) {
            found = 0;
            for (var j=0; j<data.length; j++) {
                if (this.oldData.bookmarks[i].bnumber === data[j].bnumber && this.oldData.bookmarks[i].cnumber === data[j].cnumber && this.oldData.bookmarks[i].vnumber === data[j].vnumber) {
                    found = j;
                } else {

                }
            }
            if (found === 0)
                data.push(this.oldData.bookmarks[i]);
        }
        api.restoreBookmarks(data, enyo.bind(this, this.callbackRestore, $L("Old Bookmarks")));
        this.$.oldBackupInput.setValue("");
    },

    openFilePicker: function (inSender, inEvent) {
        this.$.filepicker.pickFile();
    },
    handleFilePicker: function (inSender, files) {
        for (var i=0;i<files.length;i++) {
            if (files[i].fullPath.search("biblezBookmarks") != -1) {
                this.$.backupService.readFile(files[i].fullPath, enyo.bind(this, this.callbackReadFile, "bookmarks"));
            } else if (files[i].fullPath.search("biblezNotes") != -1) {
                this.$.backupService.readFile(files[i].fullPath, enyo.bind(this, this.callbackReadFile, "notes"));
            } else if (files[i].fullPath.search("biblezHighlights") != -1) {
                this.$.backupService.readFile(files[i].fullPath, enyo.bind(this, this.callbackReadFile, "highlights"));
            }
        }
    },

    callbackReadFile: function (inType, inResponse) {
        //this.$.btBackup.setActive(false);
        //enyo.log("RESPONSE:", inType, inResponse);
        if (inResponse.returnValue) {
            switch (inType) {
                case "bookmarks":
                    api.restoreBookmarks(enyo.json.parse(inResponse.content), enyo.bind(this, this.callbackRestore, $L("Bookmarks")));
                break;
                case "notes":
                    api.restoreNotes(enyo.json.parse(inResponse.content), enyo.bind(this, this.callbackRestore, $L("Notes")));
                break;
                case "highlights":
                    api.restoreHighlights(enyo.json.parse(inResponse.content), enyo.bind(this, this.callbackRestore, $L("Highlights")));
                break;
            }
        }
    },

    callbackRestore: function (inType) {
        //enyo.log("RESTORE", inType);
        enyo.windows.addBannerMessage($L("Restored") + " " + inType, enyo.json.stringify({}));
    },

    handleGlobalOption: function (respone) {
        enyo.log(respone);
    }
});